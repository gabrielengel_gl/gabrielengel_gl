# gabrielengel_gl

I am a big fan of GitLab's 👁 Transparency value. I'm a public person by default, and I'm excited to share how I work. This document was inspired by @jreporter's and @swiskow's README's.

- [GitLab Profile](https://gitlab.com/gabrielengel_gl/gabrielengel_gl)
- [Hosted Runners Directions Page](https://about.gitlab.com/direction/verify/hosted_runners/)
- [Hosted Runners HQ Epic](https://gitlab.com/groups/gitlab-org/-/epics/9969)

## Who Am I 

I'm Gabriel, Senior Product Manager for [Hosted Runners](https://about.gitlab.com/direction/verify/hosted_runners/) at GitLab.

I've always wanted to be an inventor since I was a kid. Now I'm an inventor of digital products. I am driven by my passion for fast and efficient software development, technology, and kickass products that create real user value. 💥

I studied computer science and found out during my first internship that my passion was product management. Since then, I've had various PM roles and over 8 years of experience working on products. In one of my previous roles, I was a customer of GitLab and was responsible for running a self-managed GitLab instance with a 10K Ultimate licence. In this role, I transitioned my company from many point solutions to using GitLab on a single DevSecOps platform. In this role, I gained a lot of technical experience implementing infrastructure and GitLab CI/CD templates at scale, which really helps me both understand the technical persona better, but also emphasise that to our enterprise customers and buyers.

When it comes to products, I am a perfectionist who focuses on the details while loving the technical challenges. I like to get my hands dirty from time to time and jump into coding as it is the way I feel most connected to our customers. I'm a very creative and intuitive person, but I'm also a big beliver in first principles thinking and data driven decision making.

## Outside of Work

Apart from tech, I'm broadly interested in many different things.

I love surfing 🏄🏼 and spend a lot of my time travelling around the workld. You might see me in the water on my beautiful GitLab-coloured Channel Island mid-length. Also I love cooking 👨‍🍳, grappling 🤼 and am a huge coffee nerd ☕. I have a Rocket Mozzafiatto espresso machine, and have recently bought the ridiculous Weber Workshops EG-1 grinder and am absolutely in awe. I've also recently started my running journey 🏃 as I signed up for my first 10km run.

## How I work

- My standard timezone is [CET](https://time.is/CET), but I often travel to surf and will work from different timezones.
- My days are non-linear.
- If I'm in a colder place like Germany, I will work between 9AM-12AM on any given day.
- If I'm near the ocean, I'm an early riser and will work between 5am and 9pm.
- If I have tasks that take less than 5 minutes, I will do it immediately.
- I need to be very organised in order to be able to think and work effectively, I use [Hosted Runners HQ Epic](https://gitlab.com/groups/gitlab-org/-/epics/9969) to structure all out work.
- I work striclty off of GitLab To-Dos and avoid email.
- I have a strong bias for async communication, but still like the social aspect of a sync meeting every once in a while.
- I have blockers in my calendar when I won't be available for calls. If there is a block you are interested in for a customer call, please reach out!

## How to reach me

- Slack: Mentioning me in Slack is best if you need a quick but simple response. Sometimes I will set my self to away when I am in deep work, where I will respond asynchronously.
- GitLab: Mentioning me in issues and MRs is the best way to get my response on something. I am super active in burning my Todos down.
- E-mail: I try to avoid emails as much as possible, but read my emails around 2 times a day.

## Personality Type

According to the 16 Personality Types Test, I'm an [ENFP-A (Campaigner)](https://www.16personalities.com/enfp-personality), which is an intuitive, sensing and exploring personality type. We tend to embrace big ideas and actions that reflect our sense of hope and goodwill towards others. Our vibrant energy can flow in many directions.

## What you can expect from me when working together

- I have a very strong action bias where I dig in and organize and focus on getting things done.
- I always look for the best ideas to win and do not mind being wrong.
- I don't mind asking stupid questions if I don't understand something.
- I have a deep technical understanding of my domain, which helps me to better understand both the customer and the engineering team.
- I'm very empathetic, which allows me to be a great communicator.
- I'm a very creative person and will strive to think outside the box.
- I have a great intuition of what works and what doesn't.
- I'm a data driven and first principles person will always try to prove or disprove my hypothesis with data.
- I'm very honest and will always speak my mind and let you know what I really think.
- I don't shy away from conflict, but like to address and resolve it as quickly as possible.

## Weak spots

- If I don't believe in something, I won't do it (or at least I'll find it difficult).
- If I don't know how to solve something, I put off problems.
- I don't shy away from conflict and focus on results, which can overwhelm others.
- I move very quickly, which can sometimes come across as impatient and not taking people with me.
- I can come across as cold or arrogant, especially on first meeting or in conflict, even if I don't mean it.
- I often approach situations as if they're broken, with a mindset of trying to fix them, rather than taking them at face value and learning from them.
- I sometimes take on other people's responsibilities so that we can move faster. While this may work in the short term, it doesn't encourage others to step up and can cause potential friction when trying to scale.
- When I feel my intentions are questioned or treated unfairly, I often react defensively and seek confrontation rather than understanding the other person's perspective.

If you observe any of the above patters, please don't hesitate to call them out and help me grow.

## How I try to improve

- I actively seek feedback and coaching for situations, especially when I'm aware of my weaknesses.
- I want to spend more time building context and asking more questions.
- I want to make others feel more heard, especially when I'm moving at a different pace.
- I want to approach problems without focusing on the solution (this is particularly hard for me because I'm a very results-oriented person).
