### GitLab runners

| Product  | Hosting GitLab | Own the runner environment | Deploy the runner | Maintain the runner |
|----------|----------------|----------------------------|-------------------|---------------------|
| SaaS runners for GitLab.com | G | G | G | G |
| Self-managed runners | G/G*/C | C | C | C |
| GCP Integration for GitLab.com | G | C | G/C | C |
| Hosted runners for GitLab Dedicated | G* | G* | G* | G* |
| Custom hosted runners for GitLab.com | G | G* | G* | G* |
| Custom hosted runners for self-managed (with GitLab Plus) | C | G* | G* | G* |

- G: GitLab multi-tenant
- G*: GitLab single-tenant
- C: Customer
- G/C: Combination of both
